﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DTOs
{
    public class CustomerDTO : IDto
    {

        public  int ID { get; set; }
        public int age { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public string Password { get; set; }
        public string newPassword { get; set; }
        public string email { get; set; }
    }
}
