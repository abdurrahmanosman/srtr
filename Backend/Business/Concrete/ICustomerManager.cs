﻿using Business.Abstract;
using Business.Constants;
using Core.Utilities;
using DataAccess.Abstract;
using Entities.Concrete;
using Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace Business.Concrete
{
    public class ICustomerManager : ICustomerService
    {

        ICustomerDal _customerdal;
        public ICustomerManager(ICustomerDal ICustomerDal)
        {
            _customerdal = ICustomerDal;
        }
        public IResult Add(Customer customer)
        {

            var cs = _customerdal.GetAll();
            foreach(var i in cs)
            {
                if (i.Email.Equals(customer.Email))
                {
                    return new ErrorResult("Bu Eposta zaten mevcut");
                }
            }
            if(customer.Email != null && customer.Password != null)
            {
                customer.Login = 0;
                _customerdal.Add(customer);

                return new SuccessResult("kayıt başarılı");
            }
            return new ErrorResult("basarısız kayıt");
           
        }

        public IResult DeleteMyAccount(Customer customer)
        {
            _customerdal.Delete(customer);
            return new SuccessResult("hesabınız silindi");
        }
        public IResult UpdateMyAccount(Customer customer)
        {

            customer.Login = 1;
            int count = 0;
            var cs = _customerdal.GetAll();
            foreach (var i in cs)
            {
                if (i.Email.Equals(customer.Email))
                {
                    count++;
                }
            }
            if(count > 1)
            {
                return new ErrorResult("Bu Eposta zaten mevcut");
            }
            _customerdal.Update(customer);
            return new SuccessResult("hesabınız güncellendi");
        }

        public IDataResult<List<Customer>> GetAll()
        {
            return new SuccessDataResult<List<Customer>>(_customerdal.GetAll());
        }

      
        public IDataResult<Customer> login(Customer customer)
        {
            Customer toCheck = null;
            List<Customer> cs = _customerdal.GetAll();
            
            foreach(var c in cs)
            {
             
                if (c.Email.Equals(customer.Email))
                {
                    toCheck = c;
                    break;
                }
            }
            if(toCheck != null)
            if (toCheck.Password.Equals(customer.Password))
            {
                    toCheck.Login = 1;
                    _customerdal.Update(toCheck);
                    return new SuccessDataResult<Customer>(toCheck,"giris onaylandı");
            }
            toCheck.Login = 0;
            return new ErrorDataResult<Customer>("giris basarısız");
        }

        public IDataResult<ShowCustomerInfo> getCustomerİnfo(int id)
        {
            var cs = _customerdal.GetAll();
            foreach(var i in cs)
            {
                if(i.ID == id)
                {
                    return new SuccessDataResult<ShowCustomerInfo>(_customerdal.ShowCustomerInfoByCustomerId(id), Messages.customerinfoshowed);
                }
            }
            return new ErrorDataResult<ShowCustomerInfo>("Bu id ile kullanıcı bulunamadi");

            
        }

        public IDataResult<String> ChangePassword(CustomerDTO customer)
        {
           
            var result = _customerdal.get(p => p.Email == customer.email);
            if(result == null)
            {
                return new ErrorDataResult<String>("Email bulunamadı");
            }
            if (!result.Password.Equals(customer.Password))
            {
                return new ErrorDataResult<String>("Eski şifre yanlış");
            }
           
            result.Password = customer.newPassword;
            _customerdal.Update(result);

            return new SuccessDataResult<String>(result.Password,"Şifreniz değişti");



        }

        public IResult bmıcalculate(CustomerDTO customer)
        {
            List<Customer> cs = _customerdal.GetAll();
            foreach (var item in cs)
            {
                if(customer.ID == item.ID)
                {
                    int weight = customer.weight;
                    double height = customer.height/100.0;

                    double vki = (weight /Math.Pow(height,2));
                    
                    item.BodyMassİndex = Convert.ToString(vki);
                    _customerdal.Update(item);
                    if (Convert.ToDouble(item.BodyMassİndex) < 30)
                    {
                        return new SuccessResult("normal kilodasınız :" + item.BodyMassİndex );
                    }
                    return new SuccessResult("kilolusunuz :" + item.BodyMassİndex );
                   
                }
                
            }
            return new ErrorResult("yalnış boy ve kilo bilgileri girdiniz");
        }

        public IResult calculatecalory(CustomerDTO customer)
        {
            double age = Convert.ToDouble(customer.age);
            double height = Convert.ToDouble(customer.height);
            double weight = Convert.ToDouble(customer.weight);
            if (customer.height > 0 && customer.weight > 0)
            {
                Console.WriteLine("Height :{0} Weight: {1} Age: {2}",height,weight,age);
                var calory = 66 + (10 * weight) + (6.25 * height) - (5 * age)+5;
                return new SuccessResult("günlük kalori ihtiyacınız : " + calory);
            }
                   
                

            
            return new ErrorResult("bilgilerinizi yalnış girdiniz");
        }

        public IResult cıkısyap(Customer customer)
        {
          
            List<Customer> customer2 = _customerdal.GetAll();
            foreach (var c in customer2)
            {

                if (c.Email.Equals(customer.Email))
                {

                    c.Login = 0;
                    _customerdal.Update(c);
                    return new SuccessResult("cıkıs yapıldı");
                }
            }
            return new ErrorResult("yalnıs bir eposta adresi girdiniz");


        }
    }
}
